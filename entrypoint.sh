#!/bin/bash
set -e
set -o errexit
set -o pipefail


show_help() {
    echo """
Usage: docker run <imagename> COMMAND
Commands
bash    : Start a bash shell
build   : build [TYPE] [ENVIRONMENT]
deploy  : deploy [ENVIRONMENT] [optional:TAG]
"""
}


case "$1" in
    bash)
        /bin/bash "${@:2}"
    ;;
    plan)
        if [ "$#" -eq 3 ]; then
            cd /infrastructure/"$2"
            terraform init
            terraform workspace select "$3"
            terraform plan
        else
            echo 3 arguments need to be passed: build [TYPE] [ENVIRONMENT]
            exit 1
        fi
    ;;
    build)
        if [ "$#" -eq 3 ]; then
            cd /infrastructure/"$2"
            terraform init
            terraform workspace select "$3"
            terraform apply --auto-approve
        else
            echo 3 arguments need to be passed: build [TYPE] [ENVIRONMENT]
            exit 1
        fi
    ;;
    provision)
        if [ "$#" -eq 2 ]; then
            cd /provisioning
            ansible-playbook configure_ssh.yml
            ansible-playbook -i inventory/"$2" betterbets.yml -t provision
        else
            echo 2 arguments are required: provision [ENVIRONMENT]
            exit 1
        fi
    ;;
    deploy)
        if [ "$#" -eq 3 ] ; then
            cd /provisioning
            ansible-playbook configure_ssh.yml
            ansible-playbook -i inventory/"$2" betterbets.yml -t "$3"
        else
            echo 3 arguments are required: deploy [ENVIRONMENT] [TAG]
            exit 1
        fi
    ;;
    destroy)
        if [ "$#" -eq 3 ]; then
            cd /infrastructure/"$2"
            terraform init
            terraform workspace select "$3"
            terraform destroy --auto-approve
        else
            echo 3 arguments need to be passed: destroy [TYPE] [ENVIRONMENT]
            exit 1
        fi
    ;;
    *)
        show_help
        exit 1
    ;;
esac
