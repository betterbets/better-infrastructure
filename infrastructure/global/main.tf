terraform {
  # Ensure you have already created this s3 bucket
  # Access and secret keys are taken from the environment
  backend "s3" {
    bucket = "betterbets-terraform-state"
    key    = "global/terraform.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  # Access and secret keys are taken from the environment
  region = "${var.region}"
}

resource "aws_key_pair" "deploy" {
  # This is currently Chris's key pair. This should be gitlab's deploy key
  key_name = "chris-work"

  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCwcdE5l07+no9EccvG4ZDoi9EM3+3DY0QQgGsETMamogHsthiQOJBsDPHrWR57KI8QJxn4d+kaBRpFqexn4XziFUGrJEVgRyAXY3Qt7fwSHFDdU6o+aoRTZfH8eiPpBe4UYF6JE9KXsMOQHuIza4HLHJ/agxuB17Kteq6BBCT08yzgYYkJvjJQ72TbR+i5HyB72OMctAQeolLcIA2AW4iTYfmcHLopT4mrtzyFKD99zsjPFZP1UPfYrJzIivu8F7i5JjApO6hy11KbdKJeb3jYLWjCSp6/+OLo75nwcwe6f4R2pk1L7QT/z5ZvidzEBfljKoU7ouBbwTs2jnGlafB7BdZBkVHzBkInfHN6paWZRhzEL5txEllAd2uZqzQ/3zlsfMQLfKfKP5ODK7JPBHRxaIW41gNuR/BYHDbWuutSLae9niM8cVB/JwfMyqZjzXrvcBBoDqUanJ2TsPo6l3DQwvKcXQ9n48WGMnEA2mLfc0e29PPJZkXh0bZmJVeA+40aVXzLEIMkW7dHQuCNEV49qntGfFz6gazQyfzxOpV1nx5cfzZ9r5lku09R7QIQDJ1MhVUym6tOkYW1QveoIlZBQccEnh6gyo7rF4qgOLQXhoAp889CR55Nudvd/TpaGwiQ2N8Cng85fJCVenPH6t0tRt3uYq0JPzUf8bLmZPcBKQ== chrisherrmann@chrisherrmann-pc"
}

resource "aws_security_group" "lb_security_group" {
  name        = "application-load-balancer"
  description = "HTTP ECS service"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all HTTPS IPv4"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all HTTP IPv4"
  }

  egress {
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "-1"
    description = "Allow out to anywhere"
  }
}

resource "aws_security_group" "worker_security_group" {
  name        = "web-server"
  description = "Allow access from ELB and SSH"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow SSH"
  }

  ingress {
    from_port       = 8000
    to_port         = 8000
    protocol        = "tcp"
    security_groups = ["${aws_security_group.lb_security_group.id}"]
    description     = "Application load balancer"
  }

  egress {
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "-1"
    description = "Allow out for apt, docker, git, pip"
  }
}

resource "aws_s3_bucket" "backups" {
  bucket = "betterbets-dashboard-backups"
  acl    = "private"

  tags {
    Name = "betterbets-dashboard-backups"
  }
}

resource "aws_s3_bucket" "app_data" {
  bucket = "betterbets-dashboard-data"
  acl    = "private"

  tags {
    Name = "betterbets-dashboard-data"
  }
}

resource "aws_default_subnet" "default_a" {
  availability_zone = "${var.region}a"
}

resource "aws_default_subnet" "default_b" {
  availability_zone = "${var.region}b"
}

resource "aws_default_vpc" "main_network" {}
