output "key_name" {
  value = "${aws_key_pair.deploy.key_name}"
}

output "worker_security_group_name" {
  value = "${aws_security_group.worker_security_group.name}"
}

output "lb_security_group_id" {
  value = "${aws_security_group.lb_security_group.id}"
}

output "subnets" {
  value = ["${aws_default_subnet.default_a.id}", "${aws_default_subnet.default_b.id}"]
}

output "default_vpc_id" {
  value = "${aws_default_vpc.main_network.id}"
}
