terraform {
  # Ensure you have already created this s3 bucket
  # Access and secret keys are taken from the environment
  backend "s3" {
    bucket = "betterbets-terraform-state"
    key    = "dns/terraform.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  # Access and secret keys are taken from the environment
  region = "${var.region}"
}

##############################################
# Zones
##############################################
resource "aws_route53_zone" "main" {
  name = "sportsflare.io"
}

##############################################
# AWS
##############################################
resource "aws_route53_record" "aws_validation" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "_a0296b9aa2383a9eb8fc6c065794798e.sportsflare.io."
  type    = "CNAME"
  ttl     = "300"
  records = ["_53d6f250060a2c5adc7079752ac44f63.acm-validations.aws."]
}

resource "aws_route53_record" "production_acm_validation" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "_4329440a49c1322d927690a93aeae7ea.dashboard.sportsflare.io."
  type    = "CNAME"
  ttl     = "300"
  records = ["_6333a0b7ac59752c8b46555cfb38eecb.acm-validations.aws."]
}

resource "aws_route53_record" "staging_acm_validation" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "_6bbe4f31429a01e8c5017a6537b219dd.www.sportsflare.io."
  type    = "CNAME"
  ttl     = "300"
  records = ["_62c83a320ff85a62d79d35f5ea2fe2ed.acm-validations.aws."]
}

resource "aws_route53_record" "domain_key" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "k1._domainkey.sportsflare.io."
  type    = "TXT"
  ttl     = "300"
  records = ["k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDeXtjYtYbuWi0gYFqIIGKlSN/QO95cFnVNnB2qFuq+sdFY9YeRrL3fBnpUEjq4YiEarEFRZiOW+IgmR8z866+wpf2SrpvQfHl+hRvPvUGboBEbeuOr3ppkyi+vOYMX1liA89nXo0YwlvY/157j3sRK0EnTdT3D9My7aMBTlijHNQIDAQAB"]
}

##############################################
# Google
##############################################
resource "aws_route53_record" "website_google_validation" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "ulajzgkstnbe.sportsflare.io."
  type    = "CNAME"
  ttl     = "300"
  records = ["gv-gq3nbxxlzizxy7.dv.googlehosted.com"]
}

resource "aws_route53_record" "dashboard_google_validation" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "mdrzmvckrddl.dashboard.sportsflare.io."
  type    = "CNAME"
  ttl     = "300"
  records = ["gv-5ftt7polppcxem.dv.googlehosted.com"]
}

resource "aws_route53_record" "google_mail" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "${aws_route53_zone.main.name}"
  type    = "MX"
  ttl     = "300"

  records = [
    "5 ALT1.ASPMX.L.GOOGLE.COM.",
    "5 ALT2.ASPMX.L.GOOGLE.COM.",
    "1 ASPMX.L.GOOGLE.COM.",
    "10 ALT3.ASPMX.L.GOOGLE.COM.",
    "10 ALT4.ASPMX.L.GOOGLE.COM.",
  ]
}

##############################################
# Mailgun and Mailchimp
##############################################
resource "aws_route53_record" "mailgun_txt" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "${aws_route53_record.main.fqdn}"
  type    = "TXT"
  ttl     = "300"

  # Include both mailgun and mailchimp
  records = ["v=spf1 include:mailgun.org include:servers.mcsv.net ~all"]
}

resource "aws_route53_record" "mailgun_cname" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "email.sportsflare.io."
  type    = "CNAME"
  ttl     = "300"
  records = ["mailgun.org"]
}

##############################################
# Website
##############################################
resource "aws_route53_record" "main" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "${aws_route53_zone.main.name}"
  type    = "A"
  ttl     = "3600"
  records = ["52.59.222.96"]
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "www.${aws_route53_record.main.fqdn}"
  type    = "A"
  ttl     = "3600"
  records = ["52.59.222.96"]
}
