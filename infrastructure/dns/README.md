# DNS


## Overview

All DNS entries associated with the sportsflare.io zone are to be entered here. This means the zone is totally managed
by Terraform.
