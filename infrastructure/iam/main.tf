terraform {
  # Ensure you have already created this s3 bucket
  # Access and secret keys are taken from the environment
  backend "s3" {
    bucket = "betterbets-terraform-state"
    key    = "iam/terraform.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  # Access and secret keys are taken from the environment
  region = "${var.region}"
}
