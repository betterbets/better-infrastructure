# Global


## Overview

Any configuration that is common across all applications and workspaces.

 - Security groups
 - Buckets
 - Subnets
 - Key Pairs
