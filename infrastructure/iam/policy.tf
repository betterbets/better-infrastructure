data "aws_iam_policy_document" "ec2_trust_policy" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com"
      ]
    }
  }
}

data "aws_iam_policy_document" "betterbets_ec2" {
  statement {
    effect = "Allow"
    actions = [
      "lambda:InvokeFunction",
      "s3:GetObject",
      "s3:PutObject",
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "betterbets_ec2" {
  name   = "BetterbetsEC2"
  path   = "/"
  policy = "${data.aws_iam_policy_document.betterbets_ec2.json}"
}
