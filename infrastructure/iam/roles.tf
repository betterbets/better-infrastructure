resource "aws_iam_role" "betterbets_ec2" {
name = "BetterbetsEc2ServiceRole"
assume_role_policy = "${data.aws_iam_policy_document.ec2_trust_policy.json}"
description = "Allows Betterbets EC2 instances to call AWS services on your behalf."
max_session_duration = 43200
}

resource "aws_iam_instance_profile" "betterbets_ec2" {
name="BetterbetsEc2InstanceProfile"
role = "${aws_iam_role.betterbets_ec2.name}"
}

resource "aws_iam_role_policy_attachment" "betterbets_ec2" {
role = "${aws_iam_role.betterbets_ec2.name}"
policy_arn = "${aws_iam_policy.betterbets_ec2.arn}"
}
