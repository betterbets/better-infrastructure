# Infrastructure

## Overview

The dashboard infrastructure is built in AWS using Terraform and is divided into 3 parts: Dashboard, DNS and Global.
Terraform is used to keep all of our infrastructure as code, meaning any changes that need to be made in AWS, must be
done via Terraform.

### Global

Global infrastructure contains everything that will be common across different environments (staging, production). This
includes:

 - Security Groups
 - Object Storage
 - Networks
 - Key Pairs

### Dashboard

All the infrastructure required for hosting the Dashboard application. This includes:

 - Instance
 - Volumes
 - Load balancer

### DNS

Finally, the DNS of the sportsflare.io zone is managed using Amazon's "Route53". This contains all the records
currently required for the Dashboard application.
