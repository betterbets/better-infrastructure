resource "aws_route53_record" "dashboard-record" {
  name    = "${terraform.workspace == "default" ? "" : format("%v.", terraform.workspace)}dashboard.${data.terraform_remote_state.dns.zone_name}"
  type    = "CNAME"
  zone_id = "${data.terraform_remote_state.dns.zone_id}"
  ttl     = "3600"
  records = ["${aws_lb.betterbets-dashboard-lb.dns_name}"]
}

resource "aws_route53_record" "dashboard-worker" {
  name    = "dashboard-worker-${terraform.workspace == "default" ? "production" : terraform.workspace}.${data.terraform_remote_state.dns.zone_name}"
  type    = "A"
  zone_id = "${data.terraform_remote_state.dns.zone_id}"
  ttl     = "300"
  records = ["${aws_instance.worker.public_ip}"]
}
