resource "aws_instance" "worker" {
  ami               = "${data.aws_ami.ubuntu.id}"
  instance_type     = "${var.instance_type}"
  key_name          = "${data.terraform_remote_state.global.key_name}"
  security_groups   = ["${data.terraform_remote_state.global.worker_security_group_name}"]
  availability_zone = "${var.region}a"
  iam_instance_profile = "${data.terraform_remote_state.iam.ec2_instance_profile}"

  tags {
    Name = "dashboard-worker-${terraform.workspace == "default" ? "production" : terraform.workspace}"
  }
}

resource "aws_ebs_volume" "postgres_volume" {
  availability_zone = "${var.region}a"
  size              = 30

  tags {
    Name = "${terraform.workspace == "default" ? "production" : terraform.workspace}_postgres_data"
  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name  = "${var.ebs_device_name}"
  volume_id    = "${aws_ebs_volume.postgres_volume.id}"
  instance_id  = "${aws_instance.worker.id}"
  skip_destroy = true
}

resource "aws_lb_target_group" "betterbets-dashboard-tg" {
  name     = "dashboard-${terraform.workspace == "default" ? "production" : terraform.workspace}-tg"
  port     = 8000
  protocol = "HTTP"
  vpc_id   = "${data.terraform_remote_state.global.default_vpc_id}"

  health_check {
    protocol            = "HTTP"
    path                = "/login"
    port                = "8000"
    healthy_threshold   = "5"
    unhealthy_threshold = "2"
    timeout             = "2"
    interval            = "30"
    matcher             = "200"    # Allow redirect, home page redirects to login
  }
}

resource "aws_lb_target_group_attachment" "lb-tg" {
  target_group_arn = "${aws_lb_target_group.betterbets-dashboard-tg.arn}"
  target_id        = "${aws_instance.worker.id}"
  port             = 8000
}

resource "aws_lb" "betterbets-dashboard-lb" {
  name            = "dashboard-${terraform.workspace == "default" ? "production" : terraform.workspace}-lb"
  internal        = false
  security_groups = ["${data.terraform_remote_state.global.lb_security_group_id}"]
  subnets         = ["${data.terraform_remote_state.global.subnets}"]

  tags {
    Environment = "${terraform.workspace == "default" ? "production" : terraform.workspace}"
  }
}

resource "aws_lb_listener" "betterbets-dashboard-https" {
  load_balancer_arn = "${aws_lb.betterbets-dashboard-lb.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${terraform.workspace == "default" ? var.certificate : var.wildcard_certificate}"

  default_action {
    target_group_arn = "${aws_lb_target_group.betterbets-dashboard-tg.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener" "betterbets-dashboard-http" {
  load_balancer_arn = "${aws_lb.betterbets-dashboard-lb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.betterbets-dashboard-tg.arn}"
    type             = "forward"
  }
}
