# Dashboard


## Overview

Build all infrastructure required to run the Dashboard application.

### EC2 Instance

Instance on which docker-compose is run.

### Load Balancer

SSL termination is done at the load balancer using an Amazon Certificate. This routes all traffic through to the Nginx
container which proxies requests through to the Django application.

###  Block Storage

Data volume to be mounted into the EC2 instance to persist data storage.
