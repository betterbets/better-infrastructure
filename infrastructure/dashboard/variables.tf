variable "region" {
  default = "us-west-2"
}

variable "instance_type" {
  default = "t3.medium"
}

variable "ebs_data_name" {
  default = "data"
}

variable "ebs_device_name" {
  default = "/dev/sdh"
}

variable "certificate" {
  default = "arn:aws:acm:us-west-2:646163240173:certificate/c43fa563-b682-430b-8c70-8a24b0c3ad0d"
}

variable "wildcard_certificate" {
  default = "arn:aws:acm:us-west-2:646163240173:certificate/7ef8c6b4-cc7a-4675-af60-009550abf04d"
}
