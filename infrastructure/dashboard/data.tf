data "terraform_remote_state" "global" {
  backend = "s3"

  config {
    bucket = "betterbets-terraform-state"
    key    = "global/terraform.tfstate"
    region = "us-west-2"
  }
}

data "terraform_remote_state" "iam" {
  backend = "s3"

  config {
    bucket = "betterbets-terraform-state"
    key    = "iam/terraform.tfstate"
    region = "us-west-2"
  }
}

data "terraform_remote_state" "dns" {
  backend = "s3"

  config {
    bucket = "betterbets-terraform-state"
    key    = "dns/terraform.tfstate"
    region = "us-west-2"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-20180126"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

