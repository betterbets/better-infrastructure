# Provisioning

## Overview

Provisioning is done using Ansible following the [Ansible Best Practices guide](http://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)
This means that any changes that need to be made to the production or staging servers must be done via Ansible.


## Playbooks

The Ansible playbooks are divided up in two. One is used for provisioning the worker machines, and the other for
deployments.

Each of the roles have tags associated with them to increase the precision for which you can run the playbooks. For
example:

 -  Provisioning just the base

    ```bash
    ansible-playbook -i hosts/production provision.yml -t base
    ```

 -  Deploying just the image

    ```bash
    ansible-playbook -i hosts/production deploy.yml -t image
    ```

The full ist of tags are:

#### Provision

 - base
 - betterbets
 - docker
 - gitlab

#### Deploy

 - configure
 - models
 - image


## Variables

Each of the variables are divided up into _hosts_ and _groups_. Hosts being `staging` and `production` and groups being
`aws`, `betterbets` and `gitlab`.
