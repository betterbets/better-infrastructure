# GitLab


## Overview

This role configures a server to be able to talk to GitLab to clone repositories and pull Docker images.


## Variables

 - deploy_public_key
 - deploy_secret_key
 - docker_registry_token
 - docker_registry_url
 - docker_registry_user
 - home_path
