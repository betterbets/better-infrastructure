# Docker


## Overview

This role sets up and configures Docker and Docker Compose for Ubuntu systems.


## Dependencies

 - base


## Variables

 - docker_
 - docker_compose_url
