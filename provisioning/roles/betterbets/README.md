# Betterbets


## Overview

This role is responsible for anything related to the Dashboard application itself. This means it will:

 - Clone.pull the better-deploy repository
 - Ensure the Docker Compose environment variables file is created
 - Configure volumes for Postgres data
 - Pull backup Docker image and configure cron to run it periodically


## Dependencies

 - base


## Variables

 - deploy_repo_url
 - docker_backup_image_name
 - home_path
 - notification_email
 - postgres_mount_path
