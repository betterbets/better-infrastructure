# Dashboard Infrastructure
Terraform and Ansible scripts responsible for maintaining all of the Dashboard infrastructure in AWS.


## Overview

The Dashboard infrastructure is divided into two main parts. Building infrastructure in AWS with Terraform, and
provisioning the servers with Ansible.

Any changes to infrastructure must be done via Terraform and any changes to the software on the server must be done
via Ansible.

The running of Terraform and Ansible is done using GitLab CI.

More detailed information about the implementation of the Infrastructure and Provisioning an be found in the README.md
files in each respective sub-directory.


## Running Terraform Locally

Terraform can be run locally via Docker.

```bash
docker run -it -e AWS_ACCESS_KEY_ID=123abc -e AWS_SECRET_ACCESS_KEY=123abc --rm registry.gitlab.com/betterbets/better-infrastructure plan [resource|(dashboard,dns,global,iam)] [environment|(default,staging)]
```

**Note:** Deployments must be done via GitLab
