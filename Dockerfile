FROM python:3.6-alpine3.7

# Build ARGs
ARG TERRAFORM_VERSION=0.11.7
ARG TERRAFORM_SHA256SUM=6b8ce67647a59b2a3f70199c304abca0ddec0e49fd060944c26f666298e23418

RUN apk add --update --no-cache \
  # ---- Build Dependencies ----
  --virtual build-dependencies \
   gcc \
   g++ \
   libffi-dev \
   libstdc++ \
   linux-headers \
   make \
   musl-dev \
   openssl-dev \
 # ---- System Dependencies ----
 && apk add --no-cache \
  bash \
  curl \
  git \
  openssh \
  openssl \
 # ------- Setup Python --------
 && pip install ansible==2.5.5 \
 # ----- Install Terraform -----
 && curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip > terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
 && echo "${TERRAFORM_SHA256SUM}  terraform_${TERRAFORM_VERSION}_linux_amd64.zip" > terraform_${TERRAFORM_VERSION}_SHA256SUMS \
 && sha256sum -cs terraform_${TERRAFORM_VERSION}_SHA256SUMS \
 && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin \
 && rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
 && apk --purge del build-dependencies

COPY ./entrypoint.sh /entrypoint.sh
COPY ./provisioning /provisioning
COPY ./infrastructure /infrastructure

ENTRYPOINT ["bash", "/entrypoint.sh"]
CMD ["bash"]
